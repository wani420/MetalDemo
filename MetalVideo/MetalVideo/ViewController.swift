//
//  ViewController.swift
//  MetalVideo
//
//  Created by Wani Wang on 2018/6/6.
//  Copyright © 2018年 Wani. All rights reserved.
//

import UIKit
import AVFoundation
import MetalKit
import CoreVideo

class ViewController: UIViewController {
    
    lazy var metalView: MetalView = {
        let v = MetalView(frame: view.bounds, device: nil)
        view.addSubview(v)
        return v
    }()
    
    lazy var playerItemVideoOutput: AVPlayerItemVideoOutput = {
        let outputAttributes = [kCVPixelBufferPixelFormatTypeKey as String : Int(kCVPixelFormatType_32BGRA)]
        let outputItem = AVPlayerItemVideoOutput(pixelBufferAttributes: outputAttributes)
        return outputItem
    }()
    
    lazy var player: AVPlayer = {
        let videoURL = Bundle.main.url(forResource: "snow", withExtension: "MP4")!
        let asset = AVURLAsset(url: videoURL)
        let playerItem = AVPlayerItem(asset: asset)
        playerItem.add(playerItemVideoOutput)

        return  AVPlayer(playerItem: playerItem)
    }()
    
    lazy var displayLink: CADisplayLink = {
        let dl = CADisplayLink(target: self, selector: #selector(ViewController.render))
        dl.add(to: .current, forMode: .defaultRunLoopMode)
        return dl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        displayLink.isPaused = false
        player.play()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        metalView.frame = view.bounds
        
    }

    @objc func render(_ sender: CADisplayLink) {
        var currentTime = kCMTimeInvalid
        let nextVSync = sender.timestamp + sender.duration
        currentTime = playerItemVideoOutput.itemTime(forHostTime: nextVSync)
        
        if playerItemVideoOutput.hasNewPixelBuffer(forItemTime: currentTime), let pixelBuffer = playerItemVideoOutput.copyPixelBuffer(forItemTime: currentTime, itemTimeForDisplay: nil) {
            self.metalView.pixelBuffer = pixelBuffer
            self.metalView.inputTime = currentTime.seconds
        }

    }
}

class MetalView: MTKView {
    
    var inputTime: CFTimeInterval?
    
    var pixelBuffer: CVPixelBuffer? {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override init(frame frameRect: CGRect, device: MTLDevice?) {
        
        // Get the default metal device.
        let metalDevice = MTLCreateSystemDefaultDevice()!
        
        // Create a command queue.
        self.commandQueue = metalDevice.makeCommandQueue()!
        
        // Create a function with a specific name.
        let function = metalDevice.makeDefaultLibrary()!.makeFunction(name: "colorKernel")!
        
        // Create a compute pipeline with the above function.
        self.computePipelineState = try! metalDevice.makeComputePipelineState(function: function)
        
        // Initialize the cache to convert the pixel buffer into a Metal texture.
        var textCache: CVMetalTextureCache?
        if CVMetalTextureCacheCreate(kCFAllocatorDefault, nil, metalDevice, nil, &textCache) != kCVReturnSuccess {
            fatalError("Unable to allocate texture cache.")
        }
        else {
            self.textureCache = textCache
        }
        
        // Initialize super.
        super.init(frame: frameRect, device: device)

        // Assign the metal device to this view.
        self.device = metalDevice
        
        // Enable the current drawable texture read/write.
        self.framebufferOnly = false
        
        // Disable drawable auto-resize.
        self.autoResizeDrawable = false
        
        // Set the content mode to aspect fit.
        self.contentMode = .scaleAspectFit
        
        // Change drawing mode based on setNeedsDisplay().
        self.enableSetNeedsDisplay = true
        self.isPaused = true
        
        // Set the content scale factor to the screen scale.
        self.contentScaleFactor = UIScreen.main.scale
        
        // Set the size of the drawable.
        self.drawableSize = frame.size
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var textureCache: CVMetalTextureCache?
    private var commandQueue: MTLCommandQueue
    private var computePipelineState: MTLComputePipelineState

    
    override func draw(_ rect: CGRect) {
        autoreleasepool {
            if rect.width > 0 && rect.height > 0 {
                self.render(self)
            }
        }
    }
    
    func render(_ view:MTKView) {
        // Check if the pixel buffer exists
        guard let pixelBuffer = self.pixelBuffer else { return }
        
        // Get width and height for the pixel buffer
        let width = CVPixelBufferGetWidth(pixelBuffer)
        let height = CVPixelBufferGetHeight(pixelBuffer)
        
        // Converts the pixel buffer in a Metal texture.
        var cvTextureOut: CVMetalTexture?
        CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, self.textureCache!, pixelBuffer, nil, .bgra8Unorm, width, height, 0, &cvTextureOut)
        guard let cvTexture = cvTextureOut, let inputTexture = CVMetalTextureGetTexture(cvTexture) else {
            assertionFailure("Failed to create metal texture")
            return
        }
        
        // Check if Core Animation provided a drawable.
        guard let drawable: CAMetalDrawable = self.currentDrawable else { return }
        
        // Create a command buffer
        let commandBuffer = commandQueue.makeCommandBuffer()!
        
        // Create a compute command encoder.
        let computeCommandEncoder = commandBuffer.makeComputeCommandEncoder()!
        
        // Set the compute pipeline state for the command encoder.
        computeCommandEncoder.setComputePipelineState(computePipelineState)
        
        // Set the input and output textures for the compute shader.
        computeCommandEncoder.setTexture(inputTexture, index: 0)
        computeCommandEncoder.setTexture(drawable.texture, index: 1)
        
        // Convert the time in a metal buffer.
        var time = Float(self.inputTime!)
        computeCommandEncoder.setBytes(&time, length: MemoryLayout<Float>.size, index: 0)
        
        // Encode a threadgroup's execution of a compute function
        computeCommandEncoder.dispatchThreadgroups(inputTexture.threadGroups(), threadsPerThreadgroup: inputTexture.threadGroupCount())
        
        // End the encoding of the command.
        computeCommandEncoder.endEncoding()
        
        // Register the current drawable for rendering.
        commandBuffer.present(drawable)
        
        // Commit the command buffer for execution.
        commandBuffer.commit()
    }
}

extension MTLTexture {
    func threadGroupCount() -> MTLSize {
        return MTLSizeMake(8, 8, 1)
    }
    
    func threadGroups() -> MTLSize {
        let groupCount = threadGroupCount()
        return MTLSizeMake(Int(self.width) / groupCount.width, Int(self.height) / groupCount.height, 1)
    }
}
